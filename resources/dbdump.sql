PGDMP     &                    w            userDB    10.1    10.1                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       1262    16774    userDB    DATABASE     �   CREATE DATABASE "userDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE "userDB";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16824    logs    TABLE     �   CREATE TABLE logs (
    id integer NOT NULL,
    date timestamp without time zone,
    log_level character varying(10),
    message character varying(255),
    exception character varying(255)
);
    DROP TABLE public.logs;
       public         postgres    false    3            �            1259    16822    logs_id_seq    SEQUENCE     |   CREATE SEQUENCE logs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.logs_id_seq;
       public       postgres    false    3    198                       0    0    logs_id_seq    SEQUENCE OWNED BY     -   ALTER SEQUENCE logs_id_seq OWNED BY logs.id;
            public       postgres    false    197            �            1259    16791    role    TABLE     �   CREATE TABLE role (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    description character varying(255)
);
    DROP TABLE public.role;
       public         postgres    false    3            �            1259    16841 	   user_role    TABLE     p   CREATE TABLE user_role (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);
    DROP TABLE public.user_role;
       public         postgres    false    3            �            1259    16839    user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.user_role_id_seq;
       public       postgres    false    200    3                       0    0    user_role_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;
            public       postgres    false    199            �            1259    16861    users    TABLE     �   CREATE TABLE users (
    id integer NOT NULL,
    uname character varying(255) NOT NULL,
    birthday date,
    login_id character varying(255),
    city character varying(40),
    email character varying(60),
    description character varying(255)
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16859    users_id_seq    SEQUENCE     }   CREATE SEQUENCE users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    3    202                       0    0    users_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE users_id_seq OWNED BY users.id;
            public       postgres    false    201            �
           2604    16827    logs id    DEFAULT     T   ALTER TABLE ONLY logs ALTER COLUMN id SET DEFAULT nextval('logs_id_seq'::regclass);
 6   ALTER TABLE public.logs ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    198    198            �
           2604    16844    user_role id    DEFAULT     ^   ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);
 ;   ALTER TABLE public.user_role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    199    200            �
           2604    16864    users id    DEFAULT     V   ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    202    202                      0    16824    logs 
   TABLE DATA               @   COPY logs (id, date, log_level, message, exception) FROM stdin;
    public       postgres    false    198   m                 0    16791    role 
   TABLE DATA               .   COPY role (id, name, description) FROM stdin;
    public       postgres    false    196   @                 0    16841 	   user_role 
   TABLE DATA               2   COPY user_role (id, user_id, role_id) FROM stdin;
    public       postgres    false    200   �       
          0    16861    users 
   TABLE DATA               Q   COPY users (id, uname, birthday, login_id, city, email, description) FROM stdin;
    public       postgres    false    202   �                  0    0    logs_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('logs_id_seq', 31, true);
            public       postgres    false    197                       0    0    user_role_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('user_role_id_seq', 5, true);
            public       postgres    false    199                       0    0    users_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('users_id_seq', 24, true);
            public       postgres    false    201            �
           2606    16795    role Role_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY role
    ADD CONSTRAINT "Role_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.role DROP CONSTRAINT "Role_pkey";
       public         postgres    false    196            �
           2606    16832    logs logs_pkey 
   CONSTRAINT     E   ALTER TABLE ONLY logs
    ADD CONSTRAINT logs_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.logs DROP CONSTRAINT logs_pkey;
       public         postgres    false    198            �
           2606    16846    user_role user_role_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_pkey;
       public         postgres    false    200            �
           2606    16869    users users_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    202               �  x�Ŕ�N�@���)���E�kb�Oĭ�&� �K��&�\i�/P��Z�y�{��3cM0�%�X�̽�=ߜ��VްSV>��$휰�+S�����Ʈ0�&4�w
h�q�q ��#���}$}$���k�-����ӧ�gb��i�6O׭U���]�Tv*N�1*�Ό�/T��t�mn�3�9u�Ѐ/��PД|L}!��, q�Q��T��������R���b>B�;��8,5��"Rl�c�|����2o{�?�(��K����>���r3�<�w%k�H��Q�K@�Rz��e�d�,�%��^hY�\T)JM�ZB�"�Y9��<�<�������:z��8���MB��s�^�T�z9m��bq�h��m�_��z�R�����F���+�b��PC��&�琥c��J��ݪyF�� C8;s�12�|t�YU�s��㉶聙H$� ׬�r         ?   x�3�t��L�+)�L��\���)��y��%E�%��y�� n1�1�SfNNf^:g�.����� ��            x�3�4�4�2�&`�L��qqq 5g      
     x����J�0�ϓ��dI�mko��E�� xɦq�&҆
>�ӮB�S��|�?��Aܛ�7�dlך�J*!��k���Y���`|�qXb޸ɎLÛ��]�͠��B�B�5�Ѷft7�ވ��j�cF~.��3��B�J](��џQ�g���4�ŧ���@�&��}��8vp�h;�2��J>����Ч�clv��/��Yx2��nJn�$6]0h|Ӎ�d�7����T��؎DUFF���*�jA�Z�
���{I����T������q`����$     