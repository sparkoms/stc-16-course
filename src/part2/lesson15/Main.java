package part2.lesson15;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;


/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws IOException, SQLException {

        Connection connection = null;
        Savepoint savepoint = null;
        Properties properties = new Properties();
        properties.load(new FileInputStream("resources\\config.properties"));

        try {
            connection = DriverManager.getConnection(properties.getProperty("db"), properties);
            Statement statement = connection.createStatement();

            /**
             * 2a Через jdbc интерфейс сделать запись данных(INSERT) в таблицу используя параметризированный запрос
             */
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, 4);
            preparedStatement.setString(2, "David Backham");
            preparedStatement.setDate(3, new Date(2001, 5, 23));
            preparedStatement.setString(4, "David");
            preparedStatement.setString(5, "London");
            preparedStatement.setString(6, "david@gmail.com");
            preparedStatement.setString(7, "dav descr");
            preparedStatement.execute();

            /**
             * 2b Через jdbc интерфейс сделать запись данных(INSERT) в таблицу используя batch процесс
             */
            statement.addBatch("INSERT INTO users VALUES (1, 'Vick Leontev', '21.05.2000', 'Vick', 'Sochi', 'vick@gmail.com', 'la-la-la')");
            statement.addBatch("INSERT INTO users VALUES (2, 'Lazar Angelov', '11.02.1990', 'Lazar', 'Sofia', 'lazar@gmail.com', 'bla-bla-bla')");
            statement.addBatch("INSERT INTO users VALUES (3, 'Brad Pitt', '07.01.1960', 'Brad', 'New-York', 'brad@gmail.com', 'descriptions')");
            statement.executeBatch();

            /**
             * 3 Сделать параметризированную выборку по login_id и uname одновременно
             */
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM users WHERE login_id = ? AND uname = ?");
            preparedStatement1.setString(1, "Brad");
            preparedStatement1.setString(2, "Brad Pitt");
            ResultSet resultSet = preparedStatement1.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("uname");
                String email = resultSet.getString("email");
                System.out.println("id = " + id + " name = " + name + " email = " + email);
            }

            /**
             * 4 Перевести connection в ручное управление транзакциями
             * 4ab Выполнить 2-3 SQL операции на ваше усмотрение
             * (например, Insert в 3 таблицы – USER, ROLE, USER_ROLE) между sql операциями установить точку сохранения (SAVEPOINT A),
             * намеренно ввести некорректные данные на последней операции, что бы транзакция откатилась к логической точке SAVEPOINT A
             */
            connection.setAutoCommit(false);
            statement.executeUpdate("INSERT INTO users VALUES (5, 'Elton John', '23.07.1956', 'Elton', 'Manchester', 'elton@gmail.com', 'elt-elt description')");
            statement.executeUpdate("INSERT INTO user_role VALUES (1, 5, 1)");
            savepoint = connection.setSavepoint("savepoint");
            statement.executeUpdate("INSERT INTO users VALUES (6, 'Johnny Depp', '19.11.1980', 'Johnny', 'Las Vegas', 'johnny@gmail.com', 'johnny description')");

//           Вставляем заведомо ложные данные (вместо целого значения String)
            statement.executeUpdate("INSERT INTO user_role VALUES (2, 6, 'str')");
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQLException. Executing rollback to savepoint...");
            connection.rollback(savepoint);
            connection.commit();
            connection.close();
        }
    }
}