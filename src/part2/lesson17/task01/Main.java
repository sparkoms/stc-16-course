package part2.lesson17.task01;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        UserManagement management = null;
        RoleUserManagement roleUserManagement = null;
        Savepoint savepoint = null;
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("resources\\config.properties"));
            Connection connection = DriverManager.getConnection(properties.getProperty("db"), properties);
            management = new UserManagement(connection);
            roleUserManagement = new RoleUserManagement(connection);

            management.userParamQuery(new User("Dav Backham", new Date(83, 5, 23), "David", "London", "david@gmail.com", "dav describe"));


            User[] users = {new User("Vick Leontev", new Date(100, 2, 22), "Vick", "Sochi", "vick@gmail.com", "la-la-la"),
                            new User("Lazar Angelov", new Date(90, 3, 22), "Lazar", "Sofia", "lazar@gmail.com", "bla-bla-bla"),
                            new User("Brad Pitt", new Date(60, 11, 23), "Brad", "New-York", "brad@gmail.com", "descriptions")
            };
            management.runBatch(users);

            ResultSet resultSet = management.selectByUserLogin("Vick Leontev", "Vick");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("uname");
                String email = resultSet.getString("email");
                System.out.println("id = " + id + " name = " + name + " email = " + email);
            }

            management.getConnection().setAutoCommit(false);

            int userId = management.userParamQuery(new User("Elton John", new Date(82, 11, 19), "Elton", "Manchester", "elton@gmail.com", "elt-elt description"));
            roleUserManagement.roleParamQuery(new RoleUser(userId, 1));
            savepoint = management.getConnection().setSavepoint("savepoint");
            int userId2 = management.userParamQuery(new User("Johnny Depp", new Date(80, 11, 12), "Johnny", "Las Vegas", "johnny@gmail.com", "johnny description"));
            roleUserManagement.roleParamQuery(new RoleUser(userId2, 5));
            if(true) throw new SQLException("Неверный код роли!");
            management.getConnection().commit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            management.getConnection().rollback(savepoint);
            management.getConnection().commit();
            e.printStackTrace();
            management.closeAll();
            roleUserManagement.closeAll();
        }
    }
}