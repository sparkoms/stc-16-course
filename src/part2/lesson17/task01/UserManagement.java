package part2.lesson17.task01;

import java.sql.*;

/**
 * Класс управления сущностями БД пользователей
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class UserManagement {

    /**
     * Свойство - соединение
     */
    private Connection connection;

    /**
     * Свойство - запрос
     */
    private PreparedStatement preparedStatement;

    /**
     * Свойство - строка запроса
     */
    private String sqlString = "";

    /**
     * Свойство - результирующий набор данных
     */
    private ResultSet resultSet;

    /**
     * Конструктор - получение данных о соединении
     */
    public UserManagement(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод получения значения поля connection {@link UserManagement#connection}
     * @return соединение с БД
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Метод определения значения поля connection {@link UserManagement#connection}
     * @param connection - соединение с БД
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод - находит по идентификатору пользователя
     * @param id - идентификатор записи
     * @return набор данных, удовлетворяющих идентификатору
     */
    public ResultSet getUserById(int id) throws SQLException {
        sqlString = "SELECT * FROM users WHERE id = ?";
        preparedStatement = connection.prepareStatement(sqlString);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    /**
     * Метод - вставляет в БД данные пользователя
     * @param user - сущность пользователя
     * @return id - идентификатор вставленной записи
     */
    public int userParamQuery(User user) throws SQLException {
        sqlString = "INSERT INTO users (uname, birthday, login_id, city, email, description) VALUES (?, ?, ?, ?, ?, ?)";
        preparedStatement = connection.prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setDate(2, user.getBirthday());
        preparedStatement.setString(3, user.getLoginId());
        preparedStatement.setString(4, user.getCity());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setString(6, user.getDescription());
        preparedStatement.executeUpdate();
        preparedStatement.getGeneratedKeys().next();
        return preparedStatement.getGeneratedKeys().getInt("id");
    }

    /**
     * Метод - вставляет пакетно все записи о пользователях, переданных в массиве
     * @param users - массив пользователя
     * @return набор данных вставленных записей
     */
    public ResultSet runBatch(User[] users) throws SQLException {
        sqlString = "INSERT INTO users (uname, birthday, login_id, city, email, description) VALUES (?, ?, ?, ?, ?, ?)";
        preparedStatement = connection.prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS);

        for (int i = 0; i < users.length; i++) {
            preparedStatement.setString(1, users[i].getUsername());
            preparedStatement.setDate(2, users[i].getBirthday());
            preparedStatement.setString(3, users[i].getLoginId());
            preparedStatement.setString(4, users[i].getCity());
            preparedStatement.setString(5, users[i].getEmail());
            preparedStatement.setString(6, users[i].getDescription());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    return preparedStatement.getGeneratedKeys();
    }

    /**
     * Метод - выбирает из БД данные пользователя по имени и логину пользователя
     * @param username - имя пользователя
     * @param loginId - логин пользователя
     * @return набор данных, соответствующих запросу
     */
    public ResultSet selectByUserLogin (String username, String loginId) throws SQLException {
        sqlString = "SELECT * FROM users WHERE login_id = ? AND uname = ?";
        preparedStatement = connection.prepareStatement(sqlString);
        preparedStatement.setString(2, username);
        preparedStatement.setString(1, loginId);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    /**
     * Метод - закрытие соединения и запроса
     */
    public void closeAll() throws SQLException {
        preparedStatement.close();
        connection.close();
    }
}