package part2.lesson17.task01;

/**
 * Класс сущности БД соответствия ролей и пользователей
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class RoleUser {
    /**
     * Свойство - идентификатор таблицы
     */
    private int id;
    /**
     * Свойство - идентификатор пользователя
     */
    private int userId;
    /**
     * Свойство - идентификатор роли
     */
    private int roleId;

    /**
     * Конструктор - получение данных
     */
    public RoleUser(int userId, int roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    /**
     * Геттеры и сеттеры полей
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}