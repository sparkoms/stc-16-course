package part2.lesson17.task01;

import java.sql.*;

/**
 * Класс управления сущностями БД ролей и пользователей
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class RoleUserManagement {

    /**
     * Свойство - соединение
     */
    private Connection connection;

    /**
     * Свойство - запрос
     */
    private PreparedStatement preparedStatement;

    /**
     * Свойство - строка запроса
     */
    private String sqlString = "";

    /**
     * Свойство - результирующий набор данных
     */
    private ResultSet resultSet;

    /**
     * Конструктор - получение данных о соединении
     */
    public RoleUserManagement(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод получения значения поля connection {@link RoleUserManagement#connection}
     * @return соединение с БД
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Метод определения значения поля connection {@link RoleUserManagement#connection}
     * @param connection - соединение с БД
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод - находит по идентификатору связь пользователя с ролью
     * @param id - идентификатор записи
     * @return набор данных, удовлетворяющих идентификатору
     */
    public ResultSet getRoleUserById(int id) throws SQLException {
        sqlString = "SELECT * FROM user_role WHERE id = ?";
        preparedStatement = connection.prepareStatement(sqlString);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    /**
     * Метод - вставляет в БД связь между пользователем и ролью
     * @param roleUser - сущность соответствия пользователя и роли
     * @return id - идентификатор вставленной записи
     */
    public int roleParamQuery(RoleUser roleUser) throws SQLException {
        sqlString = "INSERT INTO user_role (user_id, role_id) VALUES (?, ?)";
        preparedStatement = connection.prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, roleUser.getUserId());
        preparedStatement.setInt(2, roleUser.getRoleId());
        preparedStatement.executeUpdate();
        preparedStatement.getGeneratedKeys().next();
        return preparedStatement.getGeneratedKeys().getInt("id");
    }

    /**
     * Метод - закрытие соединения и запроса
     */
    public void closeAll() throws SQLException {
        preparedStatement.close();
        connection.close();
    }
}