package part2.lesson17.task01;

import java.sql.Date;

/**
 * Класс сущности БД пользователей
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class User {
    /**
     * Свойство - идентификатор таблицы
     */
    private int id;
    /**
     * Свойство - имя пользователя
     */
    private  String username;
    /**
     * Свойство - дата рождения пользователя
     */
    private Date birthday;
    /**
     * Свойство - логин пользователя
     */
    private String loginId;
    /**
     * Свойство - город пользователя
     */
    private String city;
    /**
     * Свойство - email пользователя
     */
    private String email;
    /**
     * Свойство - описание
     */
    private String description;

    /**
     * Конструктор - получение данных
     */
    public User(String username, Date birthday, String loginId, String city, String email, String description) {
        this.username = username;
        this.birthday = birthday;
        this.loginId = loginId;
        this.city = city;
        this.email = email;
        this.description = description;
    }

    /**
     * Геттеры и сеттеры полей
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}