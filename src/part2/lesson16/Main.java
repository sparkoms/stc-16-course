package part2.lesson16;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;


/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {

    static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws IOException, SQLException {
        Connection connection = null;
        Savepoint savepoint = null;
        Properties properties = new Properties();
        properties.load(new FileInputStream("resources\\config.properties"));

        try {
            connection = DriverManager.getConnection(properties.getProperty("db"), properties);
            logger.info("Соединение с базой данных установлено.");
            Statement statement = connection.createStatement();

            /**
             * 2a Через jdbc интерфейс сделать запись данных(INSERT) в таблицу используя параметризированный запрос
             */
            String sqlString = "INSERT INTO users (uname, birthday, login_id, city, email, description) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlString);
            preparedStatement.setString(1, "David Backham");
            preparedStatement.setDate(2, new Date(2001, 5, 23));
            preparedStatement.setString(3, "David");
            preparedStatement.setString(4, "London");
            preparedStatement.setString(5, "david@gmail.com");
            preparedStatement.setString(6, "dav descr");
            preparedStatement.execute();
            logger.info("Вставлены данные в таблицу users через параметризированный запрос");

            /**
             * 2b Через jdbc интерфейс сделать запись данных(INSERT) в таблицу используя batch процесс
             */
            statement.addBatch("INSERT INTO users (uname, birthday, login_id, city, email, description)" +
                    " VALUES ('Vick Leontev', '21.05.2000', 'Vick', 'Sochi', 'vick@gmail.com', 'la-la-la')");
            statement.addBatch("INSERT INTO users (uname, birthday, login_id, city, email, description)" +
                    " VALUES ('Lazar Angelov', '11.02.1990', 'Lazar', 'Sofia', 'lazar@gmail.com', 'bla-bla-bla')");
            statement.addBatch("INSERT INTO users (uname, birthday, login_id, city, email, description)" +
                    " VALUES ('Brad Pitt', '07.01.1960', 'Brad', 'New-York', 'brad@gmail.com', 'descriptions')");
            statement.executeBatch();
            logger.info("Вставлены строки с помощью batch процесса");

            /**
             * 3 Сделать параметризированную выборку по login_id и uname одновременно
             */
            sqlString = "SELECT * FROM users WHERE login_id = ? AND uname = ?";
            PreparedStatement preparedStatement1 = connection.prepareStatement(sqlString);
            preparedStatement1.setString(1, "Brad");
            preparedStatement1.setString(2, "Brad Pitt");
            ResultSet resultSet = preparedStatement1.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("uname");
                String email = resultSet.getString("email");
                System.out.println("id = " + id + " name = " + name + " email = " + email);
                logger.info("Параметризированная выборка выводит запись с id " + id);
            }
            /**
             * 4 Перевести connection в ручное управление транзакциями
             * 4ab Выполнить 2-3 SQL операции на ваше усмотрение
             * (например, Insert в 3 таблицы – USER, ROLE, USER_ROLE) между sql операциями установить точку сохранения (SAVEPOINT A),
             * намеренно ввести некорректные данные на последней операции, что бы транзакция откатилась к логической точке SAVEPOINT A
             */
            connection.setAutoCommit(false);
            statement.executeUpdate("INSERT INTO users (uname, birthday, login_id, city, email, description)" +
                    " VALUES ('Elton John', '23.07.1956', 'Elton', 'Manchester', 'elton@gmail.com', 'elt-elt description')");
            statement.executeUpdate("INSERT INTO user_role (user_id, role_id) VALUES (5, 1)");

            logger.info("Вставлена корректная запись в связанные таблицы");

            savepoint = connection.setSavepoint("savepoint");

            logger.info("Установлен savepoint");

            statement.executeUpdate("INSERT INTO users (uname, birthday, login_id, city, email, description)" +
                    " VALUES ('Johnny Depp', '19.11.1980', 'Johnny', 'Las Vegas', 'johnny@gmail.com', 'johnny description')");

//           Вставляем заведомо ложные данные (вместо целого значения String)
            statement.executeUpdate("INSERT INTO user_role (user_id, role_id) VALUES (6, 'str')");

            logger.info("Вставлена некорректная запись");

            connection.commit();
        } catch (SQLException e) {
            logger.error("SQLException. Ошибка!");
            e.printStackTrace();

            logger.info("Возврат к savepoint");
            connection.rollback(savepoint);
            connection.commit();

            logger.info("Изменения зафиксированы");

            connection.close();
            logger.info("Соединение закрыто");
        }
    }
}