package part1.lesson02.task01;
/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("---Hello world---");
        NPEException.generate();
        System.out.println("---Hello world---");
        IOOBException.generate();
        System.out.println("---Hello world---");
        AnotherException.generate();
    }
}
