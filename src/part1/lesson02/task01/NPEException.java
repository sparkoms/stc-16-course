package part1.lesson02.task01;
/**
 * Класс создания NullPointerException.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class NPEException {
    /** Свойство - строка для генерации исключения */
    private static String word;
    /**
     * Функция генерации исключения NullPointerException
     */
    public static void generate() {
        try {
            System.out.println(word.toUpperCase());
        } catch (NullPointerException e) {
            System.out.println("Сгенерировано NullPointerException");
        }
    }
}