package part1.lesson02.task01;

/**
 * Класс создания Exception через throw.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class AnotherException {
    /**
     * Функция генерации исключения ArithmeticException
     */
    public static void generate() {
        try {
            throw new ArithmeticException();
        } catch (ArithmeticException e) {
            System.out.println("Сгенерировано через throw ArithmeticException");
        }
    }
}
