package part1.lesson02.task01;

/**
 * Класс создания IndexOutOfBoundsException.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class IOOBException {
    /** Свойство - массив целых чисел для генерации исключения */
    private static int[] arr = new int[5];
    /**
     * Функция генерации исключения IndexOutOfBoundsException
     */
    public static void generate() {
        try {
            System.out.println(arr[5]);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Сгенерировано IndexOutOfBoundsException");
        }
    }
}
