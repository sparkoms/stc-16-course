package part1.lesson02.task02;

import java.util.ArrayList;
import java.util.Random;

/**
 * Класс для создания массива и проверки условий
 * @autor Виктор Леонтьев
 * @version 1.0
 */

public class ArrayOfNumbers {
    /**
     * Свойство - n чисел в массиве
     */
    private int n;

    /**
     * Свойство -  массив чисел
     */
    private ArrayList<Integer> numbers;

    /**
     * Создает, заполняет и выводит массив с числами
     */
    public ArrayOfNumbers() {
        Random random = new Random();
        n = 2 + random.nextInt(100);
        System.out.println("Сгенерировать массив с N = " + n + " чисел");

        numbers = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int number = -100 + random.nextInt(201);
            numbers.add(number);
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Проверяет элементы массива на соответсвие условиям задания (равенство квадрата целой части q заданному изначально числу)
     */
    public void checkResult() {
        for (int i = 0; i < n; i++) {
            System.out.println("-------------------");
            double q = Math.sqrt(numbers.get(i));
            System.out.println("q = " + q);
            try {
                if (Double.isNaN(q)) throw new ArithmeticException();
                System.out.println("Квадрат целой части равен " + (int)Math.pow((int)q, 2));
                if ((int)Math.pow((int)q, 2) == numbers.get(i)) {
                    System.out.println("Число q, квадрат целой части которого совпал с начальным числом k равно " + q);
                }
            } catch (ArithmeticException e) {
                System.out.println("Произошло ArithmeticException");
                continue;
            }
        }
    }
}