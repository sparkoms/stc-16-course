package part1.lesson02.task02;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        ArrayOfNumbers arrayOfNumbers = new ArrayOfNumbers();
        arrayOfNumbers.checkResult();
    }
}