package part1.lesson02.task03;
/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {

        Person[] persons = Person.createPersons(1000);

        Person.check(persons);

        System.out.println("----------------------Sort by Comparator------------------------");
        long start = System.currentTimeMillis();
        SortByComparator sortByComparator = new SortByComparator(persons);
        sortByComparator.sort();
        long end = System.currentTimeMillis() - start;
        System.out.println("Время выполнения сортировки компаратором составило " + end + " миллисекунд");

        System.out.println("----------------------Sort by quick sort------------------------");
        start = System.currentTimeMillis();
            SortByQuickSort sortByQuickSort = new SortByQuickSort(persons);
            sortByQuickSort.sort();
        end = System.currentTimeMillis() - start;
        System.out.println("Время выполнения быстрой сортировки составило " + end + " миллисекунд");

    }
}