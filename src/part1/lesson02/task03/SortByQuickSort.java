package part1.lesson02.task03;

/**
 * Класс сортировки на основе компаратора
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class SortByQuickSort implements Isort {

    /**
     * Свойство - массив Person
     */
    Person[] persons;

    /**
     * Конструктор - создание нового объекта с определенным значением массива
     */
    public SortByQuickSort(Person[] persons) {
        this.persons = persons;
    }


    /**
     * Метод запуска и вывода результата сортировки
     */
    @Override
    public void sort() {
        int start = 0;
        int end = persons.length - 1;
        doSort(start, end);
        for (Person person : persons) {
                System.out.println(person);
            }
    }

    /**
     * Метод адаптации результата сравзнения объектов Person к булевому значению
     * @param p1 - сравниваемый объект
     * @param p2 - сравниваемый объект
     * @return значение результата сравнения типа boolean
     */
    private boolean adaptToBoolean(Person p1, Person p2) {
        if (p1.compareTo(p2) == 1) {
            return false;
        }
        return true;
    }

    /**
     * Реализация метода быстрой сортировки
     * @param start - начальный элемент
     * @param end - конечный элемент
     */
    private void doSort(int start, int end) {
        if (start >= end)
            return;
        int i = start, j = end;
        int cur = i - (i - j) / 2;
        while (i < j) {
            while (i < cur && (adaptToBoolean(persons[i], persons[cur]))) {
                i++;
            }
            while (j > cur && (adaptToBoolean(persons[cur], persons[j]))) {
                j--;
            }
            if (i < j) {
                Person tmp = persons[i];
                persons[i] = persons[j];
                persons[j] = tmp;
                if (i == cur)
                    cur = j;
                else if (j == cur)
                    cur = i;
            }
        }
        doSort(start, cur);
        doSort(cur+1, end);
    }
}