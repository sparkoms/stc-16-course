package part1.lesson02.task03;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.Serializable;
import java.util.Objects;
import java.util.Random;

/**
 * Класс лиц со свойствами age, sex и name.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Person implements Comparable<Person>, Serializable {

    /**
     * Свойство - возраст
     */
    private Integer age;

    /**
     * Свойство - пол
     */
    private Sex sex;

    /**
     * Свойство - имя
     */
    private String name;

    /**
     * Конструктор - создание нового объекта с определенными значениями
     */
    public Person() {
        Random random = new Random();
        age = random.nextInt(101);
        sex = Sex.values()[random.nextInt(2)];
        name = RandomStringUtils.randomAlphabetic(1, 4);
    }

    /**
     * Метод получения значения поля {@link Person#age}
     * @return возвращает значение возраста
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Метод получения значения поля {@link Person#sex}
     * @return возвращает значение пола
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Метод получения значения поля {@link Person#name}
     * @return возвращает значение имени
     */
    public String getName() {
        return name;
    }

    /**
     * Функция создания массива из Person
     * @param n - размер формируемого массива
     * @return persons - возвращает массив Person
     */
    public static Person[] createPersons(int n) {

        Person[] persons = new Person[n];

        for (int i = 0; i < n; i++) {
            Person person = new Person();
            persons[i] = person;
            System.out.println(i + ". " + person);
        }
        return persons;
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        return "Name: " + name + ", Sex: " + sex + ", age: " + age;
    }

    /**
     * Переопределенный метод equals()
     * @param o - сравниваемый объект
     * @return значение boolean равенства или неравенства
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age.equals(person.age) &&
                name.equals(person.name);
    }

    /**
     * Переопределенный метод hashCode()
     * @return значение hashcode для текущей реализации по полям age и name
     */
    @Override
    public int hashCode() {
        return Objects.hash(age, name);
    }

    /**
     * Метод проверки элементов массива Person на равенство
     * @param persons - массив элементов ддля проверки
     */
    public static void check(Person[] persons) {
        for (int i = 0; i < persons.length - 1; i++) {
            for (int j = 0; j < persons.length - 1; j++) {
                if (i == j) {
                    continue;
                }
                if (persons[i].equals(persons[j])) {
                    try {
                        throw new EqualsException();
                    } catch (EqualsException e) {
                        System.out.println("Ошибка! Имя и возраст пользователя под номером " + i + " совпадают с пользователем под номером " + j);
                    }
                }
            }
        }
    }

    /**
     * Переопределенный метод compareTo()
     * @param o - сравниваемый объект
     * @return значение результата сравнения
     */
    @Override
    public int compareTo(Person o) {
        int val = sex.compareTo(o.sex);
        if (val != 0) {
            return val;
        }

        val = o.age.compareTo(age);
        if (val != 0) {
            return val;
        }

        val = this.name.compareTo(o.name);
        return  val > 0 ? 1 : val < 0 ? -1 : 0;
    }
}