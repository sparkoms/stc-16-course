package part1.lesson02.task03;

/**
 * Интерфейс с единственным определением метода сортировки.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public interface Isort {
    public void sort();
}