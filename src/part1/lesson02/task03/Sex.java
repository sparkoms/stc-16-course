package part1.lesson02.task03;

/**
 * Enum-класс для хранения постоянных значений пола
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public enum Sex {
    MAN, WOMAN
}
