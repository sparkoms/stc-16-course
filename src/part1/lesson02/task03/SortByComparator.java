package part1.lesson02.task03;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Класс сортировки на основе компаратора
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class SortByComparator implements Isort {

    /**
     * Свойство - массив Person
     */
    Person[] persons;

    /**
     * Конструктор - создание нового объекта с определенным значением массива
     */
    public SortByComparator(Person[] persons) {
        this.persons = persons;
    }

    /**
     * Метод сортировки, реализованный на основе компаратора
     */
    @Override
    public void sort() {
        Arrays.sort(persons, Comparator.comparing(Person::getSex).thenComparing(Person::getAge).thenComparing(Person::getName));
        for (Person person : persons) {
            System.out.println(person);
        }

    }
}