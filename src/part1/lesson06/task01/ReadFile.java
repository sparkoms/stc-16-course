package part1.lesson06.task01;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс чтения, сортировки и записи файла.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class ReadFile {

    /**
     * Свойство - статическая строка имени файла для чтения данных
     */
    private static String fileSrc = "source.txt";

    /**
     * Свойство - статическая строка имени файла для записи данных
     */
    private static String fileDst = "destination.txt";

    /**
     * Метод, выполняющий чтение, сортировку и запись результата в файл
     */
    public static void sortList() throws IOException {
        List<String> words = new ArrayList<>();
        for(String line : Files.readAllLines(Paths.get(fileSrc), StandardCharsets.UTF_8)) {
            for(String word : line.split("\\s+")) {
                word = word.toLowerCase();
                if(words.contains(word)) {
                    continue;
                }
                words.add(word);
            }
        }
        System.out.println("------------Начальный список слов--------------");
        System.out.println(words);
        Collections.sort(words);
        System.out.println("------------Отсортированный список слов--------------");
        System.out.println(words);
        Files.write(Paths.get(fileDst), words, StandardCharsets.UTF_8);
    }
}