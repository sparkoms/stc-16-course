package part1.lesson06.task01;

import java.io.IOException;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        try {
            ReadFile.sortList();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}