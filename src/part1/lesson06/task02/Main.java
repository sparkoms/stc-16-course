package part1.lesson06.task02;

import java.io.IOException;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws IOException {
        String[] wordArray = Generator.generateArray();

        for (String s : wordArray) {
            System.out.println(s);
        }

        Generator.getFiles("D:\\files", 5, 2100, wordArray, 1);
    }
}
