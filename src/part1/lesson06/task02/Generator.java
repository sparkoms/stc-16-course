package part1.lesson06.task02;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Класс, обеспечивающий генерацию текстовых файлов.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Generator {

    /**
     * Свойство - статический объект Random
     */
    private static Random random = new Random();

    /**
     * Метод - создание по указанному пути некоторого числа текстовых файлов определенного размера
     * @param path - путь к директории для генерации файлов
     * @param n - количество генерируемых файлов
     * @param size - размер генерируемых файлов
     * @param words - массив слов для включения в предложения генерируемых файлов
     * @param probability - число, обратно пропорциональное вероятности попадания слов из массива в предложения
     */
    public static void getFiles(String path, int n, int size, String[] words, int probability) throws IOException {

        try {
            if (probability == 0) {
                throw new ArithmeticException();
            }
        } catch (ArithmeticException e) {
            System.out.println("Probability не должно равняться нулю!");
            return;
        }

        File dir = new File(path);
        try {
            if (!dir.exists()) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Такой папки не существует");
            return;
        }
        StringBuilder result = new StringBuilder();

        if (dir.isDirectory()) {
            for (int i = 0; i < n; i++) {
                File file = new File(path + "\\" + i + ".txt");
                FileWriter writer = new FileWriter(file);
                    do {
                        result.append(generateParagraph(probability, words));
                    } while ((result.length() <= size));

                writer.write(result.substring(0, size));
                writer.close();
                result.delete(0, result.length());
            }
        }
    }

    /**
     * Метод генерации случайного слова
     * @return слово, состоящее от 1 до 15 символов
     */
    private static String generateWord() {
        return RandomStringUtils.randomAlphabetic(1,16).toLowerCase();
    }

    /**
     * Метод - создание предложений
     * @param probability - число, обратно пропорциональное вероятности попадания слов из массива в предложения
     * @param words - массив слов для вероятного включения его элементов в предложения
     * @return предложение
     */
    private static StringBuilder generateSentence(int probability, String[] words) {
        String[] commas = {" ", ", "};
        String[] enOfSentence = {". ", "! ", "? "};
        StringBuilder sentence = new StringBuilder();

        double p = (double) 1 / probability;

        int wordsCount = 1 + random.nextInt(15);
        for (int i = 0; i < wordsCount; i++) {
            sentence = sentence.append(generateWord() + commas[random.nextInt(commas.length)]);
        }
        sentence.setCharAt(0, Character.toUpperCase(sentence.charAt(0)));

        if (random.nextDouble() <= p) {
            sentence.append(words[random.nextInt(words.length)] + commas[random.nextInt(commas.length)]);
        }

        if (sentence.charAt(sentence.length()-2) == ',') {
            sentence.replace(sentence.length()-2, sentence.length(),enOfSentence[random.nextInt(enOfSentence.length)]);
        } else {
            sentence.replace(sentence.length()-1, sentence.length(), enOfSentence[random.nextInt(enOfSentence.length)]);
        }
        return sentence;
    }

    /**
     * Метод - создание абзацев
     * @param probability - число, обратно пропорциональное вероятности попадания слов из массива в предложения
     * @param words - массив слов для вероятного включения его элементов в предложения
     * @return абзац
     */
    private static StringBuilder generateParagraph(int probability, String[] words) {
        StringBuilder paragraph = new StringBuilder();
        int sentenceCount = 1 + random.nextInt(20);
        for (int i = 0; i < sentenceCount; i++) {
            paragraph = paragraph.append(generateSentence(probability, words));
        }
        paragraph.append("\n\r");
        return paragraph;
    }

    /**
     * Метод - генерация массива случайных слов
     * @return массив случайных слов
     */
    public static String[] generateArray() {
        int elementsCount = 1 + random.nextInt(10);
        String[] arrayOfWords = new String[elementsCount];
        for (int i = 0; i < elementsCount; i++) {
            arrayOfWords[i] = generateWord();
        }
        return arrayOfWords;
    }
}