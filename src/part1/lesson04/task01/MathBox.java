package part1.lesson04.task01;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

/**
 * Класс для работы с числовыми типами
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class MathBox {

    /**
     * Свойство - Коллекция HashSet объектов Number
     */
    private HashSet<Number> set;

    /**
     * Конструктор - создание новой коллекции HashSet из массива
     * @param array - массив для создания коллекции
     */
    public MathBox(Number[] array) {
        set = new HashSet<>(Arrays.asList(array));
    }

    /**
     * Метод суммирования элементов коллекции
     * @return возвращает сумму элементов коллекции
     */
    public double summator() {
        double sum = 0.0;
        for (Number elem : set) {
            sum += elem.doubleValue();
        }
        return sum;
    }

    /**
     * Метод разделения элементов коллекции на заданное число
     * @param div - заданное делимое число
     */
    public void splitter(Number div) {
        HashSet<Number> secondsSet = new HashSet<>();
        double splitelement = 0.0;
        for (Number elem : set) {
            try {
                splitelement = elem.doubleValue() / div.doubleValue();
                if (splitelement == Double.POSITIVE_INFINITY || splitelement == Double.NEGATIVE_INFINITY) {
                    throw new ArithmeticException();
                }
                secondsSet.add(splitelement);
            } catch (ArithmeticException e) {
                System.out.println("Деление на ноль невозможно!");
                break;
            }
            set = secondsSet;
        }
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        return "" + set;
    }

    /**
     * Переопределенный метод equals()
     * @param o - сравниваемый объект
     * @return значение boolean равенства или неравенства
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return set.equals(mathBox.set);
    }

    /**
     * Переопределенный метод hashCode()
     * @return значение hashcode для текущей реализации
     */
    @Override
    public int hashCode() {
        return Objects.hash(set);
    }

    /**
     * Метод удаления переданного целого числа
     * @param inum - заданное целое число
     */
    public void delInteger(Integer inum) {
        if (set.contains(inum) == true) {
            set.remove(inum);
        }
    }
}