package part1.lesson04.task01;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Integer[] arr = {1, 7, 4, 93, 26, 34};
        MathBox mathBox = new MathBox(arr);

        System.out.println(mathBox.summator());

        mathBox.delInteger(93);
        System.out.println(mathBox);

        mathBox.splitter(2);
        System.out.println(mathBox);
    }
}