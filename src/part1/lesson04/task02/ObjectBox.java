package part1.lesson04.task02;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс для работы с объектами любого типа
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class ObjectBox<T> {
    /**
     * Свойство - коллекция ArrayList объектов
     */
    private List<T> list;

    /**
     * Конструктор - создание новой коллекции ArrayList
     */
    public ObjectBox() {
        list = new ArrayList<>();
    }

    /**
     * Метод добавления объектов в коллекцию
     * @param ob - заданный объект
     */
    public void addObject(T ob) {
        list.add(ob);
    }

    /**
     * Метод удаления объектов из коллекции
     * @param ob - заданный объект
     */
    public void deleteObject(T ob) {
        if(list.contains(ob)) {
            list.remove(ob);
        }
    }

    /**
     * Метод отображения объектов в коллекции
     */
    public void dump() {
        System.out.println(list);
    }
}