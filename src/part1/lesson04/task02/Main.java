package part1.lesson04.task02;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        ObjectBox<String> stringObjectBox = new ObjectBox<>();
        stringObjectBox.addObject("Hello");
        stringObjectBox.addObject("Hi");
        stringObjectBox.addObject("My Friend");
        stringObjectBox.addObject("My Friend");
        stringObjectBox.dump();
        stringObjectBox.deleteObject("My Friend");
        stringObjectBox.dump();
    }
}
