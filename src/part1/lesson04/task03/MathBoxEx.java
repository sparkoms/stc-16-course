package part1.lesson04.task03;

import part1.lesson04.task02.ObjectBox;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Класс для работы с числовыми типами, расширяющий класс ObjectBox
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class MathBoxEx<T extends Number> extends ObjectBox<T> {

    /**
     * Свойство - коллекция HashSet объектов Number
     */
    private HashSet<T> set;

    /**
     * Конструктор - создание новой коллекции HashSet из массива
     * @param array - массив для создания коллекции
     */
    public MathBoxEx(T[] array) {
        super();
        set = new HashSet<>(Arrays.asList(array));
    }

    /**
     * Метод суммирования элементов коллекции
     * @return возвращает сумму элементов коллекции
     */
    public double summator() {
        double sum = 0.0;
        for (T elem : set) {
            sum += elem.doubleValue();
        }
        return sum;
    }

    /**
     * Метод разделения элементов коллекции на заданное число
     * @param div - заданное делимое число
     */
    public void splitter(T div) {
        HashSet<T> secondsSet = new HashSet<>();
        Double splitelement = 0.0;
        for (T elem : set) {
            try {
                splitelement = elem.doubleValue() / div.doubleValue();
                if (splitelement == Double.POSITIVE_INFINITY || splitelement == Double.NEGATIVE_INFINITY) {
                    throw new ArithmeticException();
                }
//                secondsSet.add(Double.valueOf(splitelement));
            } catch (ArithmeticException e) {
                System.out.println("Деление на ноль невозможно!");
                break;
            }
            set = secondsSet;
        }
    }

    /**
     * Метод удаления переданного целого числа
     * @param inum - заданное целое число
     */
    public void delInteger(Integer inum) {
        if (set.contains(inum) == true) {
            set.remove(inum);
        }
    }

    /**
     * Метод добавления объектов числового типа в коллекцию
     * @param ob - заданный объект
     */
    @Override
    public void addObject(T ob) {
        set.add(ob);
    }

    /**
     * Метод удаления объектов чисового типа из коллекции
     * @param ob - заданный объект
     */
    @Override
    public void deleteObject(T ob) {
        if (set.contains(ob) == true) {
            set.remove(ob);
        }
    }

    /**
     * Метод отображения объектов чмслового типа в коллекции
     */
    @Override
    public void dump() {
        System.out.println(set);
    }
}