package part1.lesson04.task03;


import part1.lesson04.task02.ObjectBox;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Integer[] arr = {1, 7, 4, 93, 26, 34};
        MathBoxEx<Integer> mathBoxEx = new MathBoxEx<>(arr);

        mathBoxEx.dump();

        mathBoxEx.addObject(45);
        mathBoxEx.deleteObject(7);

        mathBoxEx.delInteger(26);
        mathBoxEx.dump();

        System.out.println(mathBoxEx.summator());
        System.out.println("------------------------------------");

        ObjectBox<Integer> ObjectBox = new MathBoxEx<>(arr);
        ObjectBox.dump();
        ObjectBox.addObject(356);
        ObjectBox.deleteObject(93);
        ObjectBox.dump();
    }
}