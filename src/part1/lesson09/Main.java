package part1.lesson09;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        ArrayList<String> list = new ArrayList();
        String src = "src\\part1\\lesson09\\SomeClass.java";
        System.out.println("Введите построчно код программы, разделяя инструкции точкой с запятой");
        try (Scanner sc = new Scanner(System.in)) {
            String code = sc.nextLine();
            while (!code.equals("")) {
                list.add(code);
                code = sc.nextLine();
            }
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(src))) {
            writer.write("package part1.lesson09;");
            writer.write("public class SomeClass implements Worker {");
            writer.write("public void doWork() {");
            for (String s : list) {
                writer.write(s);
            }
            writer.write("}}");
        }

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, src);

        ClassLoader loader = new MyClassLoader();
        Class<?> someClass = loader.loadClass("part1.lesson09.SomeClass");
        Object someClassObj = someClass.newInstance();
        Method method = someClass.getMethod("doWork");
        method.invoke(someClassObj);
    }
}