package part1.lesson09;

/**
 * Интерфейс с единственным определением метода, выполняющего необходимую задачу.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public interface Worker {
    void doWork();
}
