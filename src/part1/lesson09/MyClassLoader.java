package part1.lesson09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Класс, являющийся собственной реализацией ClassLoader
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class MyClassLoader extends ClassLoader {

    /**
     * Метод загрузки класса
     * @param name - имя загружаемого класса
     * @return загруженный класс
     */
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if ("part1.lesson09.SomeClass".equals(name)) {
            return findClass(name);
        }
        return super.loadClass(name);
    }

    /**
     * Метод нахождения и формирования класса из байт-кода
     * @param name - имя загружаемого класса
     * @return загруженный класс
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        System.out.println("findClass starts work: " + name);
        if ("part1.lesson09.SomeClass".equals(name)) {
            try {
                byte[] bytes = Files.readAllBytes(Paths.get("src\\part1\\lesson09\\SomeClass.class"));
                return defineClass(name, bytes, 0, bytes.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return super.findClass(name);
    }
}