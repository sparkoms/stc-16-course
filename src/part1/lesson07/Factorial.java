package part1.lesson07;

import java.math.BigInteger;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Класс, обеспечивающий генерацию массива и вычисление факториала.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Factorial {

    /**
     * Свойство - статический объект ConcurrentHashMap для хранения вычисленных значений факториала
     */
    private static Map<Integer, BigInteger> cache = new ConcurrentHashMap<>();

    /**
     * Метод генерации случайного числового массива
     * @return массив типа int
     */
    public static int[] getArray() {
        Random random = new Random();
        int[] numbers = new int[random.nextInt(100)];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = 1 + random.nextInt(100);
        }
        return numbers;
    }

    /**
     * Метод - вычисление факториала для числа int с использованием кэша вычисленных значений факториала
     * @param number - число для вычисления факториала
     * @return значение факториала типа BigInteger
     */
    public static BigInteger compute(int number) {
        BigInteger fact = BigInteger.ONE;
        int best = Integer.MAX_VALUE;

        for (int key : cache.keySet()) {
            if (key <= number) {
                int delta = number - key;
                if (delta < best) {
                    best = delta;
                }
            }
        }
        if (best != Integer.MAX_VALUE) {
            fact = cache.get(number - best);
            for (int i = number; i > number - best; i--) {
                fact = fact.multiply(BigInteger.valueOf(i));
            }
        } else {
            for (int i = 2; i <= number; i++) {
                fact = fact.multiply(BigInteger.valueOf(i));
            }
        }

        cache.put(number, fact);
        return fact;
    }
}