package part1.lesson07;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("===========Создание и вывод массива=============");
        int[] numbers = Factorial.getArray();
        for (int number : numbers) {
            System.out.println(number);
        }

        System.out.println("==============Вычисление в пуле потоков================");
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        List<Future<BigInteger>> futures = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (int number : numbers) {
            futures.add(executor.submit(() -> Factorial.compute(number)));
        }
        long end = System.currentTimeMillis();
        for (Future<BigInteger> future : futures){
            System.out.println(future.get());
        }
        System.out.println("==============Время выполнения================");
        System.out.println(end - start);
        executor.shutdown();
    }
}