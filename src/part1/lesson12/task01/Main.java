package part1.lesson12.task01;

import part1.lesson02.task03.Person;

import java.util.ArrayList;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Object> objects = new ArrayList<>();
        do {
            objects.add(new Person());
            objects.add(new Person());
            objects.add(new Person());
            objects.add(new Person());
            objects.add(new Person());
            if (objects.size() % 1000 == 0) {
                objects.remove(0);
            }
            System.out.println(objects.size());
        } while (true);
    }
}