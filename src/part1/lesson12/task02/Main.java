package part1.lesson12.task02;

import javassist.CannotCompileException;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {

    static javassist.ClassPool pool = javassist.ClassPool.getDefault();

    public static void main(String[] args) throws CannotCompileException {
        for (int i = 0; i < Long.MAX_VALUE; i++) {
            Class c = pool.makeClass("part1.lesson12.task02.MetaspaceClass" + i).toClass();
        }
    }
}
