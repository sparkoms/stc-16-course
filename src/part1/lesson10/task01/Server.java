package part1.lesson10.task01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, описывающий логику работы сервера
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Server {
    /**
     * Свойство - порт сервера
     */
    public static final int SERVER_PORT = 4998;
    /**
     * Свойство - синхронизированный лист соединений
     */
    private List<Connection> connections = Collections.synchronizedList(new ArrayList<Connection>());
    /**
     * Свойство - серверный сокет
     */
    private ServerSocket server;

    /**
     * Конструктор - запуск сервера, создание подключения
     */
    public Server() {
        try {
            server = new ServerSocket(SERVER_PORT);
            System.out.println("Сервер создан на порте " + SERVER_PORT);

            while (true) {
                Socket socket = server.accept();
                Connection connection = new Connection(socket);
                connections.add(connection);
                connection.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeAll();
        }
    }

    /**
     * Приватный метод - закрытие сервера и очистка всех подключений
     */
    private void closeAll() {
        try {
            server.close();

            synchronized(connections) {
                for (Connection connection : connections) {
                    connection.close();
                }
            }
        } catch (Exception e) {
            System.err.println("Ошибка при закрытии сервера!");
        }
    }

    /**
     * Приватный класс, описывающий каждое отдельное соединение
     */
    private class Connection extends Thread {
        /**
         * Свойство - поток чтения сокета
         */
        private BufferedReader in;
        /**
         * Свойство - поток записи в сокет
         */
        private PrintWriter out;
        /**
         * Свойство - сокет подключения
         */
        private Socket socket;

        /**
         * Свойство - ник пользователя
         */
        private String nick = "";

        /**
         * Конструктор - инициализация потоков ввода-вывода, сокета
         */
        public Connection(Socket socket) {
            this.socket = socket;

            try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

            } catch (IOException e) {
                e.printStackTrace();
                close();
            }
        }

        /**
         * Метод получения ника
         */
        public String getNick() {
            return nick;
        }

        /**
         * Переопределенный метод run() запуска потока
         */
        @Override
        public void run() {
            try {
                nick = in.readLine();
                synchronized(connections) {
                    for (Connection connection : connections) {
                        connection.out.println(nick + " теперь в чате!");
                    }
                }

                String message = "";
                while (true) {
                    message = in.readLine();
                    if (message.equals("quit")) {
                        break;
                    }

                    if (message.startsWith("To")) {
                        Pattern pattern = Pattern.compile("^To\\s{1}([a-zA-Z0-9]+)\\s{1}(\\S+\\s*)*");
                        Matcher matcher = pattern.matcher(message);

                        if (matcher.matches()) {
                            String messageToClient = message.substring(3, message.length());
                            String nameClient = messageToClient.substring(0, messageToClient.indexOf(" "));

                            for (Connection connection : connections) {
                                if (connection.getNick().equals(nameClient)) {
                                    connection.out.println(nick + ": " + messageToClient);
                                    break;
                                }
                            }
                        }
                    } else {
                        synchronized(connections) {
                            for (Connection connection : connections) {
                                connection.out.println(nick + ": " + message);
                            }
                        }
                    }
                }
                synchronized(connections) {
                    for (Connection connection : connections) {
                        connection.out.println(nick + " вышел из чата.");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }

        /**
         * Метод - закрытие ресурсов
         */
        public void close() {
            try {
                in.close();
                out.close();
                socket.close();

                connections.remove(this);

            } catch (Exception e) {
                System.err.println("Невозможно корректно закрыть соединение");
            }
        }
    }
}