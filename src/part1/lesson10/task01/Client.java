package part1.lesson10.task01;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Класс, описывающий логику работы клиента
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Client {
    /**
     * Свойство - поток чтения сокета
     */
    private BufferedReader in;
    /**
     * Свойство - поток записи в сокет
     */
    private PrintWriter out;
    /**
     * Свойство - клиентский сокет
     */
    private Socket socket;

    /**
     * Конструктор = создание клиента. Инициализация потоков ввода-вывода. Ввод ника.
     * Запись-чтение сокета.
     */
    public Client() {
        Scanner scan = new Scanner(System.in);

        try {
            socket = new Socket("localhost", Server.SERVER_PORT);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            System.out.println("Пожалуйста укажите Ваш ник:");
            out.println(scan.nextLine());

            ClientReader clientReader = new ClientReader();

            String str = "";
            while (!str.equals("quit")) {
                str = scan.nextLine();
                out.println(str);
            }
            clientReader.setStop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    /**
     * Приватный метод - закрытие ресурсов
     */
    private void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            System.err.println("Завершено с ошибками!");
        }
    }

    /**
     * Приватный класс - параллельное чтение из сокета
     */
    private class ClientReader extends Thread {

        /**
         * Конструктор - запуск потока
         */
        public ClientReader() {
            start();
        }

        /**
         * Свойство - флаг чтения
         */
        private boolean isStoped;

        /**
         * Метод остановки чтения
         */
        public void setStop() {
            isStoped = true;
        }

        /**
         * Переопределенный метод run() запуска потока
         */
        @Override
        public void run() {
            try {
                while (!isStoped) {
                    String str = in.readLine();
                    System.out.println(str);
                }
            } catch (IOException e) {
                System.err.println("Ошибка чтения сокета!");
                e.printStackTrace();
            }
        }
    }
}