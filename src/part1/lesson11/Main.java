package part1.lesson11;

import part1.lesson02.task03.Isort;
import part1.lesson02.task03.Person;

import java.util.Arrays;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    /**
     * Сортировка с помощью лямбда-выражения и stream API (lesson02.task03)
     */
    public static void main(String[] args) {
        Person[] persons = Person.createPersons(1000);
        Isort sortPerson = () -> Arrays.stream(persons).sorted().forEach(System.out::println);
        sortPerson.sort();
    }
}