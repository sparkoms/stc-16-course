package part1.lesson08.task02;

import java.io.*;
import java.lang.reflect.Field;

/**
 * Класс, выполняющий встроенную сериализацию на основе интерфейса Serializable
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class HardSerial {

    /**
     * Метод - сериализация определенного объекта в файл
     * @param object - объект для сериализации
     * @param file - файл для записи байтов
     */
    public static void serialize (Object object, String file) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(object);
        }
    }

    /**
     * Метод - десериализация объекта из файла
     * @param file - файл для десериализации
     * @return десериализованный объект
     */
    public static Object deSerialize(String file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            Object o = in.readObject();
            return o;
        }
    }
}