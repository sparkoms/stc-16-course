package part1.lesson08.task02;

import part1.lesson05.task01.Pet;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Класс, содержащий как примитивные типы, так и ссылочные
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Hard implements Externalizable {
    private int anInt;
    private long aLong;
    private double aDouble;
    private float aFloat;
    private boolean aBoolean;
    private String string;

    private Pet pet;

    /**
     * Пустой конструктор
     */
    public Hard() {

    }

    /**
     * Конструктор
     */
    public Hard(int anInt, long aLong, double aDouble, float aFloat, boolean aBoolean, String string, Pet pet) {
        this.anInt = anInt;
        this.aLong = aLong;
        this.aDouble = aDouble;
        this.aFloat = aFloat;
        this.aBoolean = aBoolean;
        this.string = string;
        this.pet = pet;
    }

    /**
     * Переопределенный метод writeExternal()
     * @param out - определяет, какие переменные будут сериализованы
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(anInt);
        out.writeObject(aLong);
        out.writeObject(aDouble);
        out.writeObject(aFloat);
        out.writeObject(aBoolean);
        out.writeObject(string);
        out.writeObject(pet);
    }

    /**
     * Переопределенный метод writeExternal()
     * @param in - определяет, какие переменные будут десериализованы
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        anInt = (int)in.readObject();
        aLong = (long)in.readObject();
        aDouble = (double)in.readObject();
        aFloat = (float)in.readObject();
        aBoolean = (boolean)in.readObject();
        string = (String)in.readObject();
        pet = (Pet)in.readObject();
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        return "Hard{" +
                "anInt=" + anInt +
                ", aLong=" + aLong +
                ", aDouble=" + aDouble +
                ", aFloat=" + aFloat +
                ", aBoolean=" + aBoolean +
                ", string='" + string + '\'' +
                ", pet=" + pet +
                '}';
    }
}