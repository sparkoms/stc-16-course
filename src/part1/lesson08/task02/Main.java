package part1.lesson08.task02;

import part1.lesson02.task03.Person;
import part1.lesson05.task01.Pet;

import java.io.IOException;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Pet pet = new Pet("Chelsea", new Person(), 1.25);
        Hard hard = new Hard(34, 36732, 55.4, 8775.23F, true, "Hello", pet);
        HardSerial.serialize(hard,"ser.txt");
        System.out.println(HardSerial.deSerialize("ser.txt"));
    }
}
