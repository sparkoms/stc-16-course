package part1.lesson08.task01;

import part1.lesson02.task03.Person;
import part1.lesson05.task01.Pet;

import java.io.IOException;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Pet pet = new Pet("Chelsea", new Person(), 1.25);
        Simple simple = new Simple(34, 567L, 3.89, 224.67F, true, "Hello", pet);
        Serial.serialize( simple, "file.txt");
        System.out.println(Serial.deSerialize("file.txt"));
    }
}