package part1.lesson08.task01;

import part1.lesson05.task01.Pet;

import java.io.*;

/**
 * Класс, содержащий как примитивные типы, так и ссылочные
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Simple implements Externalizable {
    private int anInt;
    private long aLong;
    private double aDouble;
    private float aFloat;
    private boolean aBoolean;
    private String string;

    private Pet pet;

    /**
     * Пустой конструктор
     */
    public Simple() {

    }

    /**
     * Конструктор
     */
    public Simple(int anInt, long aLong, double aDouble, float aFloat, boolean aBoolean, String string, Pet pet) {
        this.anInt = anInt;
        this.aLong = aLong;
        this.aDouble = aDouble;
        this.aFloat = aFloat;
        this.aBoolean = aBoolean;
        this.string = string;
        this.pet = pet;
    }

    /**
     * Переопределенный метод writeExternal()
     * @param out - определяет, какие переменные будут сериализованы
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(anInt);
        out.writeObject(aLong);
        out.writeObject(aDouble);
        out.writeObject(aFloat);
        out.writeObject(aBoolean);
        out.writeObject(string);
    }

    /**
     * Переопределенный метод writeExternal()
     * @param in - определяет, какие переменные будут десериализованы
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        anInt = (int)in.readObject();
        aLong = (long)in.readObject();
        aDouble = (double)in.readObject();
        aFloat = (float)in.readObject();
        aBoolean = (boolean)in.readObject();
        string = (String)in.readObject();
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        return "Simple{" +
                "anInt=" + anInt +
                ", aLong=" + aLong +
                ", aDouble=" + aDouble +
                ", aFloat=" + aFloat +
                ", aBoolean=" + aBoolean +
                ", string='" + string + '\'' +
                ", pet=" + pet +
                '}';
    }
}