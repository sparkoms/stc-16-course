package part1.lesson05.task01;

import part1.lesson02.task03.Person;

import java.io.Serializable;
import java.util.Objects;

/**
 * Класс питомцев Pet.
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Pet implements Serializable {
    /**
     * Свойство - статический счетчик для присвоения уникального идентификатора
     */
    private static int counter = 0;
    /**
     * Свойство - уникальный идентификатор питомца
     */
    private int id;
    /**
     * Свойство - имя питомца
     */
    private String petName;
    /**
     * Свойство - объект класса Person, владелец питомца
     */
    private Person person;
    /**
     * Свойство - значение веса питомца
     */
    private double weight;

    /**
     * Конструктор - создание нового объекта класса Pet с определенными значениями свойств
     * @param petName - имя питомца
     * @param person - объект класса Person, владелец питомца
     * @param weight - значение веса питомца
     */
    public Pet(String petName, Person person, double weight) {
        this.id = counter++;
        this.petName = petName;
        this.person = person;
        this.weight = weight;
    }

    /**
     * Метод получения значения поля id {@link Pet#id}
     * @return уникальный идентификатор питомца
     */
    public int getId() {
        return id;
    }

    /**
     * Метод получения имени питомца {@link Pet#petName}
     * @return имя питомца
     */
    public String getPetName() {
        return petName;
    }

    /**
     * Метод определения имени питомца {@link Pet#petName}
     * @param petName - имя питомца
     */
    public void setPetName(String petName) {
        this.petName = petName;
    }

    /**
     * Метод получения владельца питомца {@link Pet#person}
     * @return владелец питомца
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Метод определения владельца питомца {@link Pet#person}
     * @param person - владелец питомца
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     * Метод получения веса питомца {@link Pet#weight}
     * @return вес питомца
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Метод определения веса питомца {@link Pet#weight}
     * @param weight - вес питомца
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", petName='" + petName + '\'' +
                ", person=[" + person +
                "], weight=" + weight +
                '}';
    }

    /**
     * Переопределенный метод equals()
     * @param o - сравниваемый объект
     * @return значение boolean равенства или неравенства
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return id == pet.id &&
                Double.compare(pet.weight, weight) == 0 &&
                petName.equals(pet.petName) &&
                person.equals(pet.person);
    }

    /**
     * Переопределенный метод hashCode()
     * @return значение hashcode для текущей реализации
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, petName, person, weight);
    }
}