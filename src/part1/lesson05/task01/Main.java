package part1.lesson05.task01;

import part1.lesson02.task03.Person;

/**
 * Main-класс выполнения
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("----------Вывод владельцев------------");
        Person[] persons = Person.createPersons(6);

        Pet pet = new Pet("Tuzik", persons[2], 4.3);
        Pet pet1 = new Pet("Bobik", persons[2], 6.1);
        Pet pet2 = new Pet("Tuzik", persons[0], 3.9);
        Pet pet3 = new Pet("Murka", persons[1], 2.2);
        Pet pet4 = new Pet("Bim", persons[0], 7.2);
        Pet pet5 = new Pet("Murka", persons[1], 2.6);
        Pet pet6 = new Pet("Murka", persons[4], 5.2);
        Pet pet7 = new Pet("Murka", persons[5], 3.7);

        CollectionOfPets collectionOfPets = new CollectionOfPets();

        collectionOfPets.addPet(pet);
        collectionOfPets.addPet(pet1);
        collectionOfPets.addPet(pet2);
        collectionOfPets.addPet(pet3);
        collectionOfPets.addPet(pet4);
        collectionOfPets.addPet(pet5);
        collectionOfPets.addPet(pet6);
        collectionOfPets.addPet(pet7);

        System.out.println("----------Вывод коллекции------------");
        System.out.println(collectionOfPets);

        System.out.println("----------Изменение данных питомцев в коллекции------------");
        collectionOfPets.changePetById(0, persons[1]);
        collectionOfPets.changePetById(0, "Murka");
        collectionOfPets.changePetById(4, 3.3);
        System.out.println(collectionOfPets);

        System.out.println("----------Вывод искомого по имени питомца ------------");
        collectionOfPets.findPet("Tuzik");

        System.out.println("----------Вывод коллекции ------------");
        System.out.println(collectionOfPets);

        System.out.println("-------Отсортированный список--------");
        collectionOfPets.sort();
    }
}