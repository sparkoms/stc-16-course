package part1.lesson05.task01;

import part1.lesson02.task03.Person;

import java.util.*;

/**
 * Класс для создания и управления коллекцией питомцев
 * @autor Виктор Леонтьев
 * @version 2.0
 */
public class CollectionOfPets {

    /**
     * Свойство - коллекция HashMap объектов Pet с ключом имени питомца
     */
    private Map<String, List<Pet>> findPetMap;

    /**
     * Свойство - коллекция HashMap объектов Pet с ключом id
     */
    private Map<Integer, Pet> idPetMap;

    /**
     * Конструктор - создание новой коллекции HashSet
     */
    public CollectionOfPets() {
        findPetMap = new HashMap<>();
        idPetMap = new HashMap<>();
    }

    /**
     * Переопределенный метод toString()
     * @return возвращает строковое представление класса
     */
    @Override
    public String toString() {
        String str = "";
        for (Pet pet : idPetMap.values()) {
            str += pet + "\n";
        }
        return str;
    }

    /**
     * Метод добавления питомца в коллекцию
     * @param pet - питомец Pet
     */
    public void addPet(Pet pet) {
        if (!idPetMap.containsValue(pet)) {
            idPetMap.put(pet.getId(), pet);
            if (!findPetMap.containsKey(pet.getPetName())) {
                List<Pet> pets = new ArrayList<>();
                pets.add(pet);
                findPetMap.put(pet.getPetName(), pets);
            } else {
                findPetMap.get(pet.getPetName()).add(pet);
            }
        } else {
            try {
                throw new PetException();
            } catch (PetException e) {
                System.out.println("Невозможно добавить в картотеку дубликат!");
            }
        }
    }

    /**
     * Метод обновления питомца в коллекциях
     * @param pet - питомец Pet
     */
    private void updatePet(Pet pet) {
        idPetMap.remove(pet.getId());
        findPetMap.get(pet.getPetName()).remove(pet);
        addPet(pet);
    }

    /**
     * Метод обновления питомца в коллекциях в случае смены имени
     * @param pet - питомец Pet
     * @param oldName - старое имя питомца для поиска в коллекции
     */
    private void updatePet(Pet pet, String oldName) {
        idPetMap.remove(pet.getId());
        findPetMap.get(oldName).remove(pet);
        addPet(pet);
    }

    /**
     * Метод поиска питомца по имени
     * @param petName - имя питомца Pet
     */
    public void findPet(String petName) {
        if (findPetMap.containsKey(petName)) {
            for (Pet pet : findPetMap.get(petName)) {
                System.out.println(pet);
            }
        } else {
            System.out.println("Питомцы с данным именем отсутствуют в картотеке");
        }
    }

    /**
     * Метод изменения владельца питомца по его уникальному идентификатору
     * @param petId - уникальный идентификатор питомца Pet
     * @param petPerson - объект владельца Person питомца Pet
     */
    public void changePetById(int petId, Person petPerson) {
        Pet pet = idPetMap.get(petId);
        pet.setPerson(petPerson);
        updatePet(pet);
    }

    /**
     * Метод изменения имени питомца по его уникальному идентификатору
     * @param petId - уникальный идентификатор питомца Pet
     * @param petName - имя питомца Pet
     */
    public void changePetById(int petId, String petName) {
        Pet pet = idPetMap.get(petId);
        String oldName = pet.getPetName();
        pet.setPetName(petName);
        updatePet(pet, oldName);
    }

    /**
     * Метод изменения веса питомца по его уникальному идентификатору
     * @param petId - уникальный идентификатор питомца Pet
     * @param petWeight - вес питомца Pet
     */
    public void changePetById(int petId, double petWeight) {
        Pet pet = idPetMap.get(petId);
        pet.setWeight(petWeight);
        updatePet(pet);
    }

    /**
     * Метод сортировки, реализованный на основе компаратора. Метод сортирует сначала по владельцу, который
     * сортируется в порядке: пол (сначала мужчины), возраст, имя. Затем сортировка по имени и весу питомца.
     */
    public void sort() {
        List<Pet> sortedList = new ArrayList<>(idPetMap.values());
        sortedList.sort(Comparator.comparing(Pet::getPerson).thenComparing(Pet::getPetName).thenComparing(Pet::getWeight));
        for (Pet pet : sortedList) {
            System.out.println(pet);
        }
    }
}