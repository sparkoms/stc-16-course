package part2.lesson17.task01;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.sql.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class mockUserManagementTest {

    @Mock
    private Connection connection;

    @Mock
    private ResultSet resultSet;

    @Mock
    private PreparedStatement statement;

    private UserManagement management;
    private User user;

    @Before
    public void setUp() throws SQLException, IOException {
        when(connection.prepareStatement(any(String.class))).thenReturn(statement);
        when(connection.prepareStatement(any(String.class), anyInt())).thenReturn(statement);

        user = new User("Vera Brezhneva", new Date(78, 5, 12), "Vera", "", "vera@gmail.com", "bla-bla-bla vera");
        user.setId(1);

        management = new UserManagement(connection);

        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(user.getId());
        when(resultSet.getString("uname")).thenReturn(user.getUsername());
        when(resultSet.getDate("birthday")).thenReturn(user.getBirthday());
        when(resultSet.getString("login_id")).thenReturn(user.getLoginId());
        when(resultSet.getString("city")).thenReturn(user.getCity());
        when(resultSet.getString("email")).thenReturn(user.getEmail());
        when(resultSet.getString("description")).thenReturn(user.getDescription());

        doNothing().when(statement).setInt(anyInt(), anyInt());
        doNothing().when(statement).setDate(anyInt(), any(Date.class));
        doNothing().when(statement).setString(anyInt(), anyString());

        when(statement.executeQuery()).thenReturn(resultSet);
        when(statement.executeUpdate()).thenReturn(1);
        when(statement.getGeneratedKeys()).thenReturn(resultSet);
    }

    @Test
    public void getConnection() {
        assertEquals(connection, management.getConnection());
    }

    @Test
    public void getUserById() throws SQLException {
        resultSet = management.getUserById(user.getId());
        resultSet.next();
        assertEquals(resultSet.getInt("id"), user.getId());
    }

    @Test
    public void userParamQuery() throws SQLException {
        int id = management.userParamQuery(user);
        assertEquals(user.getId(), id);
    }

    @Test
    public void selectByUserLogin() throws SQLException {
        resultSet = management.selectByUserLogin(user.getUsername(), user.getLoginId());
        resultSet.next();
            ResultSet selectedResultSet = management.getUserById(resultSet.getInt("id"));
            selectedResultSet.next();
                assertTrue(selectedResultSet.getString("uname").equals(user.getUsername()));
                assertTrue(selectedResultSet.getString("login_id").equals(user.getLoginId()));
    }
}