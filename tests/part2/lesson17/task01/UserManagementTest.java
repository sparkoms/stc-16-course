package part2.lesson17.task01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserManagementTest {

    UserManagement management;
    ResultSet resultSet;
    Connection connection;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("resources\\config.properties"));
        connection = DriverManager.getConnection(properties.getProperty("db"), properties);
        management = new UserManagement(connection);
    }

    @AfterEach
    void tearDown() throws SQLException {
        management.getConnection().close();
        management = null;
    }

    @Test
    void getConnection() {
        assertEquals(management.getConnection(), connection);
    }

    @Test
    void userParamQuery() throws SQLException {
      assertTrue(management.getUserById(management.userParamQuery(
                new User("Sergey Lazarev", new Date(90, 12, 3), "Laz", "Moscow", "lazarev@mail.ru", "SuperStar"))).next());
    }

    @Test
    void runBatch() throws SQLException {
        User[] users = {new User("Valery Meladze", new Date(56, 3, 12), "Valer", "Tbilisi", "val@gmail.com", "vala"),
                new User("Angelina Goli", new Date(83, 5, 23), "Angela", "Los-Angeles", "goli@gmail.com", "bla-goliii"),
                new User("Alena Apina", new Date(66, 5, 27), "Alena", "Moscow", "apina@gmail.com", "descriptions Alena")
        };

        resultSet = management.runBatch(users);
        while (resultSet.next()) {
            assertTrue(management.getUserById(resultSet.getInt("id")).next());
        }
    }

    @Test
    void selectByUserLogin() throws SQLException {
        resultSet = management.selectByUserLogin("Angelina Goli", "Angela");
        while (resultSet.next()) {
            ResultSet selectedResultSet = management.getUserById(resultSet.getInt("id"));
            while (selectedResultSet.next()) {
                assertTrue(selectedResultSet.getString("uname").equals("Angelina Goli"));
                assertTrue(selectedResultSet.getString("login_id").equals("Angela"));
            }
        }
    }

    @Test
    void getUserById() throws SQLException {
       resultSet = management.getUserById(55);
       resultSet.next();
       assertEquals(55, resultSet.getInt("id"));
    }
}