package part2.lesson17.task01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class RoleUserManagementTest {

    RoleUserManagement roleUserManagement;
    ResultSet resultSet;
    Connection connection;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("resources\\config.properties"));
        connection = DriverManager.getConnection(properties.getProperty("db"), properties);
        roleUserManagement = new RoleUserManagement(connection);
    }

    @AfterEach
    void tearDown() throws SQLException {
        roleUserManagement.getConnection().close();
        roleUserManagement = null;
    }

    @Test
    void getConnection() {
        assertEquals(roleUserManagement.getConnection(), connection);
    }

    @Test
    void getRoleUserById() throws SQLException {
        resultSet = roleUserManagement.getRoleUserById(35);
        resultSet.next();
        assertEquals(35, resultSet.getInt("id"));
    }

    @Test
    void roleParamQuery() throws SQLException {
        assertTrue(roleUserManagement.getRoleUserById(roleUserManagement.roleParamQuery(
                new RoleUser(1, 3))).next());
    }
}